# RefactoringExercise

The purpose of this application is to transform trades coming from two systems (Mars and Jupiter) into a common format. The transformation is done in MarsSourceTransformer and JupiterSourceTransformer classes. Please refactor only ​MarsSourceTransformer and JupiterSourceTransformer​. ​Do not modify the model or services packages.