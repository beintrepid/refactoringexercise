package com.example.demo.transformers;

import com.example.demo.model.*;
import com.example.demo.service.CounterpartyLookupService;
import com.example.demo.service.FxConverter;

import java.lang.annotation.Target;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;

public class MarsSourceTradeTransformer {
    public TargetTrade transform(MarsSourceTrade sourceTrade) {
        TargetTrade result = new TargetTrade(sourceTrade.getId());
        sourceTrade.setDateProcessed(LocalDate.now());

        FxConverter converter = new FxConverter();

        BigDecimal notional = sourceTrade.getNotionalValue();

        String longShort = sourceTrade.getLongShort();

        BigDecimal notionalUsd = converter.convert(notional, "CHF", "USD");
        result.setNotionalUsd((notionalUsd));
        result.setSourceCurrency("CHF");

        PositionType positionType = null;
        if(longShort.equals("LONG")) {
            positionType = PositionType.Long;
        }

        if(longShort.equals("SHORT")) {
            positionType = PositionType.Short;
        }

        result.setPositionType(positionType);
        //result.setPositionType(positionType);

        if(notionalUsd.compareTo(BigDecimal.valueOf(1000001)) > 0){
            result.setLargeTradeFlag(true);
            result.setSource("Mars-Large");
        }

        String type = sourceTrade.getType();
        if(type.equals("FX-SWAP")) {
            result.setProductType(ProductType.Swap);
        }
        else if (type.equals("option")) {
            //result.setProductType(ProductType.Option);
            /*
            * Commented out because JIRA-456 required this not to be set until
            * We have better requirements from the business. It's safe not to
            * set this for now.
            * */
        }
        else{
            result.setProductType(ProductType.Other);
        }

        result.setSource("Mars");
        result.setSourceData(sourceTrade);

        HashSet<String> acceptedCurrencies = new HashSet<>();
        acceptedCurrencies.add("USD");
        acceptedCurrencies.add("GBP");
        acceptedCurrencies.add("CHF");
        acceptedCurrencies.add("SGD");
        acceptedCurrencies.add("AUD");
        //acceptedCurrencies.add("PLN");

        if(!acceptedCurrencies.contains(result.getSourceCurrency())){
            return null;
        }

        lookupCounterparty(sourceTrade, result);

        BigDecimal maxPosition = BigDecimal.ZERO;
        for (MarsPosition item: sourceTrade.getHistoricalPositions()) {
            if(item.getNotionalUsd().compareTo(BigDecimal.valueOf(1000000)) > 0){
                if (item.getNotionalUsd().compareTo(maxPosition)>0){
                    maxPosition = item.getNotionalUsd();
                }
            }
        }

        if(result.getPositionType() == null) {
            return null;
        }

        return result;
    }

    private void lookupCounterparty(MarsSourceTrade sourceTrade, TargetTrade result) {

        try{
            CounterpartyLookupService service = new CounterpartyLookupService();
            Counterparty counterparty = service.lookupCounterparty(sourceTrade.getCpCode());
            result.setCounterparty(counterparty);
        }catch(Exception ex)
        {
            //No counterparty
        }


    }

    private void lookupIssuer(MarsSourceTrade sourceTrade, TargetTrade result)
    {

    }
}
