package com.example.demo.transformers;

import com.example.demo.model.*;
import com.example.demo.service.CounterpartyLookupService;
import com.example.demo.service.FxConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.stream.Collectors;

public class JupiterSourceTradeTransformer {
    public TargetTrade transform(JupiterSourceTrade s) {

        TargetTrade r = new TargetTrade(s.getTradeId());

        r.setSourceData(s);

        FxConverter converter = new FxConverter();

        if(s.getNotional() == null){
            return null;
        }

        BigDecimal notional = s.getNotional();

        PositionType positionType = notional.compareTo(BigDecimal.ZERO) >= 0
                ? PositionType.Long : PositionType.Short;
        r.setPositionType(positionType);

        String currency = s.getCurrency();
        BigDecimal notionalGbp = converter.convert(notional, currency, "USD");
        r.setNotionalUsd(notionalGbp);
        r.setSource("Jupiter");
        r.setCurrency(currency);

        BigDecimal t = s.getPositions()
                .stream().filter(x -> x.getNotional().compareTo(BigDecimal.ZERO) > 0)
                .collect(Collectors.groupingBy(x -> x.getCurrency())).entrySet().stream()
                    .filter(x -> x.getKey().equals("USD")).map(x -> (BigDecimal)x.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        r.setPostionsSum(t);

        s.setDateProcessed(LocalDate.now());

        HashSet<String> acceptedCurrencies = new HashSet<>();
        acceptedCurrencies.add("USD");
        acceptedCurrencies.add("GBP");
        acceptedCurrencies.add("CHF");
        acceptedCurrencies.add("SGD");
        acceptedCurrencies.add("AUD");

        if(!acceptedCurrencies.contains(currency))
        {
            return null;
        }

        if(notionalGbp.compareTo(BigDecimal.valueOf(1000001)) > 0)
        {
            r.setLargeTradeFlag(true);
            r.setSource("Jupiter-Large");
        }

        String productType = s.getProductType();
        if(productType.contains("options")){
            r.setProductType(ProductType.Option);
        }

        if(productType.contains("swap")){
            r.setProductType(ProductType.Swap);
        }

        if(productType.contains("forward")){
            r.setProductType(ProductType.Forward);
        }

        lookupCounterparty(s, r);

        return null;
    }

    private PositionType getPositionType(BigDecimal notional, TargetTrade result) {

        PositionType positionType = notional.compareTo(BigDecimal.ZERO) >= 0
                ? PositionType.Long : PositionType.Short;
        result.setPositionType(positionType);

        return positionType;
    }

    private void lookupCounterparty(JupiterSourceTrade sourceTrade, TargetTrade result) {
        CounterpartyLookupService service = new CounterpartyLookupService();
        Counterparty counterparty = service.lookupCounterparty(sourceTrade.getCounterpartyCode());
        result.setCounterparty(counterparty);
    }
}
