package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class JupiterSourceTrade {

    private String tradeId;

    private String currency;

    private BigDecimal notional;

    private String productType;

    private String counterpartyCode;

    private List<JupiterPosition> positions;

    private LocalDate dateProcessed;
}
