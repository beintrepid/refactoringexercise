package com.example.demo.model;

public enum ProductType {
    Swap,
    Option,
    Forward,
    Other
}
