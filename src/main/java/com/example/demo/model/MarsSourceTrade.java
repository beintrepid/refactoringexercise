package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class MarsSourceTrade {

    private List<MarsPosition> historicalPositions;

    private String id;

    private BigDecimal notionalValue;

    private String longShort;

    private String cpCode;

    private String type;

    private LocalDate dateProcessed;
}
