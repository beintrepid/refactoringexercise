package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class JupiterPosition {
    private BigDecimal notional;
    private String currency;
}
