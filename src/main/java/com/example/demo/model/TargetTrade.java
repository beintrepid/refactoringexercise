package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TargetTrade {

    private Object sourceData;
    private String tradeId;
    private PositionType positionType;
    private BigDecimal notionalUsd;
    private String source;
    private String currency;
    private boolean largeTradeFlag;
    private ProductType productType;
    private Counterparty counterparty;
    private String sourceCurrency;
    private BigDecimal postionsSum;

    public TargetTrade(String tradeId) {

        this.tradeId = tradeId;
    }
}
